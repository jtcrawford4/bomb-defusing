package Controller;

import Service.GameService;

public class GameController {

    public void startGame(){
        GameService game = new GameService();
        game.initialize();
        game.start();
    }

}
