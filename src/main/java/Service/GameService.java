package Service;

import java.util.*;

import Exception.GameRuleException;

 /*
        https://www.reddit.com/r/dailyprogrammer/comments/5e4mde/20161121_challenge_293_easy_defusing_the_bomb/
        If you cut a white cable you can't cut white or black cable.
        If you cut a red cable you have to cut a green one
        If you cut a black cable it is not allowed to cut a white, green or orange one
        If you cut a orange cable you should cut a red or black one
        If you cut a green one you have to cut a orange or white one
        If you cut a purple cable you can't cut a purple, green, orange or white cable
        Winning combo on easy = purple, black, red, green, white, orange
        Winning combo on normal = ?
        Winning combo on hard = ?
 */

public class GameService {

    private static int numOfWires;
    private String wireToCut;
    private String previousWire = "";
    private int difficulty = 1; //default
    private List<String> wireColors = new LinkedList<>(Arrays.asList("white","black","red","green","orange","purple"));
    private List<String> wiresInPlay = new LinkedList<>();
    private String sysoutWireColors = wireColors.toString().replace("[","").replace("]","");

    public void initialize(){
        System.out.println("A bomb containing the following colored wires needs to be defused: " + sysoutWireColors);
        System.out.println("Your mission is to cut the wires in the correct order thereby saving the lives of your beloved cat, Mudge.");
        System.out.println("Press enter to continue...");
        try{
            System.in.read();
        } catch (Exception e){
            System.out.println("o_0 ??? ");
        }
        selectDifficulty();
    }

    public void start(){
        loadWires();
        Scanner scanner = new Scanner(System.in);
        do{
            try {
                System.out.println("Select wire to cut: " + wiresInPlay.toString().replace("[", "").replace("]", ""));
                wireToCut = scanner.nextLine().toLowerCase();
                if (wiresInPlay.contains(wireToCut)) {
                    checkRules(wireToCut);
                    cutWire(wireToCut);
                    previousWire = wireToCut;
                    numOfWires--;
                } else {
                    System.out.println("Invalid wire");
                }
            } catch (GameRuleException e){
                System.out.println("\n***** Massive explosion. Mudge is not amused. *****");
                System.err.println(e.getMessage());
                break; //TODO try again option?
            }
        } while(numOfWires > 0);

        if (numOfWires == 0){
            System.out.println("\n***** Congratulations! Mudge is pleased you have all your fingers and limbs intact in order to provide the maximum number of pets *****");
        }
    }

    private void cutWire(String wire){
        Iterator<String> iterator = wiresInPlay.iterator();
        while(iterator.hasNext()) {
            String currentValue = iterator.next();
            if (currentValue.equals(wire)) {
                iterator.remove();
                break;
            }
        }
    }

    private void checkRules(String currentWire) throws GameRuleException {
        if (!previousWire.isEmpty()) {
            //If you cut a white cable you can't cut white or black cable.
            if (previousWire.equals("white") && currentWire.matches("black|white")) {
                throw new GameRuleException("A black or white wire cannot be cut after a white wire");
            }
            //If you cut a red cable you have to cut a green one
            if(previousWire.equals("red") && !currentWire.equals("green")){
                throw new GameRuleException("A green wire must be cut after a red wire");
            }
            //If you cut a black cable it is not allowed to cut a white, green or orange one
            if(previousWire.equals("black") && currentWire.matches("white|green|orange")){
                throw new GameRuleException("A white, green, or orange wire cannot be cut after a black wire");
            }
            //If you cut a orange cable you should cut a red or black one
            if(previousWire.equals("orange") && !currentWire.matches("red|black")){
                throw new GameRuleException("Only a black or red wire can be cut after an orange wire");
            }
            //If you cut a green one you have to cut a orange or white one
            if (previousWire.equals("green") && !currentWire.matches("orange|white")){
                throw new GameRuleException("A white or orange wire must be cut after a green wire");
            }
            //If you cut a purple cable you can't cut a purple, green, orange or white cable
            if(previousWire.equals("purple") && currentWire.matches("purple|green|orange|white")){
                throw new GameRuleException("A purple, green, orange, or white wire cannot be cut after a purple wire");
            }
        }
    }

    private void selectDifficulty(){
        System.out.println("1 - Easy");
        System.out.println("2 - Normal");
        System.out.println("3 - Hard");
        System.out.print("Select difficulty level: ");
        Scanner scanner = new Scanner(System.in);
        do{
            try{
                String temp = scanner.next();
                if(!temp.matches("[1-3]")){
                    System.err.println("Please select 1, 2, or 3 for corresponding difficulty level");
                    selectDifficulty();
                }
                else{
                    difficulty = Integer.valueOf(temp);
                }
            } catch (Exception e){
                //eat exception?
            }
        } while (difficulty < 0 || difficulty > 3);
//        scanner.close(); //causes weird scanner issues if this one is closed
    }

    private void loadWires(){
        numOfWires = difficulty * wireColors.size();
        for(int i=0; i<difficulty; i++){
            wiresInPlay.addAll(wireColors);
        }
        System.out.println("[INFO] " + wiresInPlay.size() + " wires to play: " + wiresInPlay);
    }

}
