package Exception;

public class GameRuleException extends Exception{
    public GameRuleException(String message) {
        super(message);
    }
}
